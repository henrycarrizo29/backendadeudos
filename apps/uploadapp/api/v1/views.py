from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from ....uploadapp.models import File
from .serializers import FileSerializer


class FileUploadView(APIView):
    parser_class = (FileUploadParser,)

    def post(self, request, *args, **kwargs):
        file_serializer = FileSerializer(data=request.data)

        if file_serializer.is_valid():
            file_serializer.save()
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ListaArchivos(APIView):
    def get(self, request):
        datos = File.objects.all()
        # medico=Medico.objects.all()[:20]
        data = FileSerializer(datos, many=True).data
        return Response(data)


class ListaArchivos_id(APIView):
    def get(self, request, id, format=None):
        snippets = File.objects.filter(id=id)
        serializer = FileSerializer(snippets, many=True)
        return Response(serializer.data)
