from django.urls import path
from .views import FileUploadView, ListaArchivos, ListaArchivos_id
from django.conf import settings
from django.conf.urls.static import static

app_name = 'uploadapp'
urlpatterns = [
    # path('', FileUploadView.as_view()),
    # path('', ListaArchivos.as_view())
    path('FileUploadView/', FileUploadView.as_view(), name="FileUploadView"),
    path('ListaArchivos/', ListaArchivos.as_view(), name="ListaArchivos"),
    path('ListaArchivos_id/<int:id>/', ListaArchivos_id.as_view(), name="ListaArchivos_id"),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# if config.settings.base.DEBUG:
#   urlpatterns += static(config.settings.base.MEDIA_URL, document_root=config.settings.base.MEDIA_ROOT)
