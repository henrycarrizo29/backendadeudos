from django.contrib import admin
from django.urls import path, include

app_name = 'uploadapp'
urlpatterns = [
    path('v1/',include("apps.uploadapp.api.v1.urls", namespace="v1")),
]
