from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import File
# Register your models here.


@admin.register(File)
class File(admin.ModelAdmin):
    list_display = ["pk", "file"]
