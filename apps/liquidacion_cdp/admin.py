from django.contrib import admin
from .models import Liquidacion

# Register your models here.


class LiquidacionAdmin(admin.ModelAdmin):
    list_display = ('liq_cdp', 'liq_total', 'liq_descuento', 'liq_total_descuento', 'liq_fecha')
    search_fields = ('liq_cdp', 'liq_total')


# admin.site.register(Liquidacion)
admin.site.register(Liquidacion, LiquidacionAdmin)
