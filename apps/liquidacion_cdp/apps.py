from django.apps import AppConfig


class LiquidacionCdpConfig(AppConfig):
    name = 'liquidacion_cdp'
