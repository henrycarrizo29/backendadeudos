from django.db import models
from ..compromiso_de_pago.models import Compromiso_de_pago


class Liquidacion (models.Model):
    liq_cdp = models.ForeignKey(Compromiso_de_pago, on_delete=models.CASCADE)
    liq_total = models.DecimalField(max_digits=7, decimal_places=2, verbose_name='total')
    liq_descuento = models.PositiveIntegerField(default=0)
    liq_total_descuento = models.DecimalField(max_digits=7, decimal_places=2, verbose_name='total con descuento')
    liq_fecha = models.DateField()

    class Meta:
        ordering = ["-liq_fecha"]

    def __str__(self):
        return '%s' % (self.liq_cdp)
