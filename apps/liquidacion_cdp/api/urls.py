from django.contrib import admin
from django.urls import path, include

app_name = 'liquidacion'
urlpatterns = [
    path('v1/', include("apps.liquidacion_cdp.api.v1.urls", namespace="v1")),
]
