from django.urls import path
from .views import ListaLiquidacion, DetalleLiquidacion, LiquidacionBusca

app_name = 'liquidacion'
urlpatterns = [
    path('listaliquidacion', ListaLiquidacion.as_view()),
    path('detalleliquidacion/<int:pk>/', DetalleLiquidacion.as_view(), name="detalle liquidacion"),
    path('liquidacionbusca/<int:idcdp>/', LiquidacionBusca.as_view(), name="busca liquidacion"),
]