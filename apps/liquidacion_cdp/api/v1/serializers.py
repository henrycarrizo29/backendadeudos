from rest_framework import serializers
from ....liquidacion_cdp.models import Liquidacion


class LiquidacionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Liquidacion
        fields = '__all__'
