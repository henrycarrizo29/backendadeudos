from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status

from ....liquidacion_cdp.models import Liquidacion
from .serializers import LiquidacionSerializer


class ListaLiquidacion(APIView):
    # LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        liquidacion = Liquidacion.objects.all()
        serializer = LiquidacionSerializer(liquidacion, many=True)
        return Response(serializer.data)

    # CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = LiquidacionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalleLiquidacion(APIView):

    def get_object(self, pk):
        try:
            return Liquidacion.objects.get(pk=pk)
        except Liquidacion.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        liquidacion = self.get_object(pk)
        serializer = LiquidacionSerializer(liquidacion)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        liquidacion = self.get_object(pk)
        serializer = LiquidacionSerializer(liquidacion, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        liquidacion = self.get_object(pk)
        liquidacion.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class LiquidacionBusca(APIView):
    def get(self, request, idcdp, format=None):
        # | Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
        liq = Liquidacion.objects.filter(liq_cdp=idcdp)
        serializer = LiquidacionSerializer(liq, many=True)
        return Response(serializer.data)
