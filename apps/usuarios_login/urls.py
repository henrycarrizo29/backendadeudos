from django.urls import path
from .views import current_user, UserList, DetalleUsuarios, ListaUsuarios

urlpatterns = [
    path('current_user/', current_user),
    path('users/', UserList.as_view()),
    path('detalleusuarios/<int:usr>/', DetalleUsuarios.as_view(), name="DetalleUsuarios"),
    path('listausuarios', ListaUsuarios.as_view(), name="ListaUsuarios"),
]
