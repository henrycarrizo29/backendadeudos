from django.apps import AppConfig


class CajaMenorGastosConfig(AppConfig):
    name = 'caja_menor_gastos'
