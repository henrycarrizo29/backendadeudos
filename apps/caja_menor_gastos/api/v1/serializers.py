from rest_framework import serializers
from ....caja_menor_gastos.models import Gastos, Categoria, Fondos, InformeGasto, InformesCajaChica


class GastosSerializer(serializers.ModelSerializer):

    class Meta:
        model = Gastos
        fields = '__all__'


class CategoriaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Categoria
        fields = '__all__'


class FondosSerializer(serializers.ModelSerializer):

    class Meta:
        model = Fondos
        fields = '__all__'


class InformeGastoSerializer(serializers.ModelSerializer):

    class Meta:
        model = InformeGasto
        fields = '__all__'


class InformesCajaChicaSerializer(serializers.ModelSerializer):

    class Meta:
        model = InformesCajaChica
        fields = '__all__'
