from django.urls import path
from .views import ListaCategoria, ListaFondos, ListaGastos, ListaInformeGasto, ListaInformesCajaChica, DetalleCategoria, DetalleFondos, DetalleGastos, DetalleInformeGasto, DetalleInformesCajaChica, BuscaInformesCajaChica, BuscaEstadoInformesCajaChica, BuscaEstadoFondo, BuscaGastoInforme, BuscaGastoEstado

app_name = 'caja_menor_gastos'
urlpatterns = [
    path('listacategoria', ListaCategoria.as_view(), name="lista categoria"),
    path('detallecategoria/<int:pk>/', DetalleCategoria.as_view(), name="detalle categoria"),
    path('listafondos', ListaFondos.as_view(), name="lista fondos"),
    path('detallefondos/<int:pk>/', DetalleFondos.as_view(), name="detalle fondos"),
    path('listagastos', ListaGastos.as_view(), name="lista gastos"),
    path('detallegastos/<int:pk>/', DetalleGastos.as_view(), name="detalle gastos"),
    path('listainformegasto', ListaInformeGasto.as_view(), name="lista informegasto"),
    path('detalleinformegasto/<int:pk>/', DetalleInformeGasto.as_view(), name="detalle informegasto"),
    path('listainformecajachica', ListaInformesCajaChica.as_view(), name="lista informe caja chica"),
    path('detalleinformecajachica/<int:pk>/', DetalleInformesCajaChica.as_view(), name="detalle informe caja chica"),
    path('buscainformecajachica/<str:tipoinf>/', BuscaInformesCajaChica.as_view(), name="busca informe caja chica"),
    path('buscaestadoinformecajachica/<str:estadoinf>/', BuscaEstadoInformesCajaChica.as_view(), name="busca estado caja chica"),
    path('buscaestadofondo/<str:estadoFon>/', BuscaEstadoFondo.as_view(), name="busca estado fondo"),
    path('buscagastoinforme/<int:gastoInf>/', BuscaGastoInforme.as_view(), name="busca gasto informe"),
    path('buscagastoestado/<str:gastoEstado>/', BuscaGastoEstado.as_view(), name="busca gasto estado"),
]
