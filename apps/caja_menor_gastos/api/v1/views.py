from rest_framework.parsers import FileUploadParser
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status

from ....caja_menor_gastos.models import Gastos, Categoria, Fondos, InformeGasto, InformesCajaChica
from .serializers import GastosSerializer, CategoriaSerializer, FondosSerializer, InformeGastoSerializer, InformesCajaChicaSerializer


class ListaCategoria(APIView):
    # LISTA TODOS LOS REGISTROS DE LOS ESTUDIANTES
    def get(self, request, format=None):
        cat = Categoria.objects.all()
        serializer = CategoriaSerializer(cat, many=True)
        return Response(serializer.data)

    # CREA UN NUEVO REGISTRO ESTUDIANTE
    def post(self, request, format=None):
        serializer = CategoriaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalleCategoria(APIView):

    def get_object(self, pk):
        try:
            return Categoria.objects.get(pk=pk)
        except Categoria.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        cdp = self.get_object(pk)
        serializer = CategoriaSerializer(cdp)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        cdp = self.get_object(pk)
        serializer = CategoriaSerializer(cdp, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        cdp = self.get_object(pk)
        cdp.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ListaFondos(APIView):
    parser_class = (FileUploadParser,)

    def post(self, request, *args, **kwargs):
        file_serializer = FondosSerializer(data=request.data)

        if file_serializer.is_valid():
            file_serializer.save()
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        depositos = Fondos.objects.all()
        serializer = FondosSerializer(depositos, many=True)
        return Response(serializer.data)


class DetalleFondos(APIView):

    def get_object(self, pk):
        try:
            return Fondos.objects.get(pk=pk)
        except Fondos.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        depositos = self.get_object(pk)
        serializer = FondosSerializer(depositos)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        depositos = self.get_object(pk)
        serializer = FondosSerializer(depositos, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        depositos = self.get_object(pk)
        depositos.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


''' class DetalleDepositoBusca(APIView):
    def get(self, request, cdp, format=None):
        dep = Depositos.objects.filter(dep_cdp=cdp)
        serializer = DepositosSerializer(dep, many=True)
        return Response(serializer.data)
 '''


class ListaGastos(APIView):
    parser_class = (FileUploadParser,)

    def post(self, request, *args, **kwargs):
        file_serializer = GastosSerializer(data=request.data)

        if file_serializer.is_valid():
            file_serializer.save()
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        depositos = Gastos.objects.all()
        serializer = GastosSerializer(depositos, many=True)
        return Response(serializer.data)


class DetalleGastos(APIView):

    def get_object(self, pk):
        try:
            return Gastos.objects.get(pk=pk)
        except Gastos.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        depositos = self.get_object(pk)
        serializer = GastosSerializer(depositos)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        depositos = self.get_object(pk)
        serializer = GastosSerializer(depositos, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        depositos = self.get_object(pk)
        depositos.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ListaInformeGasto(APIView):
    # LISTA TODOS LOS REGISTROS DE LOS ESTUDIANTES
    def get(self, request, format=None):
        cat = InformeGasto.objects.all()
        serializer = InformeGastoSerializer(cat, many=True)
        return Response(serializer.data)

    # CREA UN NUEVO REGISTRO ESTUDIANTE
    def post(self, request, format=None):
        serializer = InformeGastoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalleInformeGasto(APIView):

    def get_object(self, pk):
        try:
            return InformeGasto.objects.get(pk=pk)
        except InformeGasto.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        cdp = self.get_object(pk)
        serializer = InformeGastoSerializer(cdp)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        cdp = self.get_object(pk)
        serializer = InformeGastoSerializer(cdp, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        cdp = self.get_object(pk)
        cdp.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


''' class DetalleDepositoBusca(APIView):
    def get(self, request, cdp, format=None):
        dep = Depositos.objects.filter(dep_cdp=cdp)
        serializer = DepositosSerializer(dep, many=True)
        return Response(serializer.data)
 '''


class ListaInformesCajaChica(APIView):
    # LISTA TODOS LOS REGISTROS DE LOS ESTUDIANTES
    def get(self, request, format=None):
        cat = InformesCajaChica.objects.all()
        serializer = InformesCajaChicaSerializer(cat, many=True)
        return Response(serializer.data)

    # CREA UN NUEVO REGISTRO ESTUDIANTE
    def post(self, request, format=None):
        serializer = InformesCajaChicaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalleInformesCajaChica(APIView):

    def get_object(self, pk):
        try:
            return InformesCajaChica.objects.get(pk=pk)
        except InformesCajaChica.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        cdp = self.get_object(pk)
        serializer = InformesCajaChicaSerializer(cdp)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        cdp = self.get_object(pk)
        serializer = InformesCajaChicaSerializer(cdp, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        cdp = self.get_object(pk)
        cdp.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class BuscaInformesCajaChica(APIView):
    def get(self, request, tipoinf, format=None):
        dep = InformesCajaChica.objects.filter(cc_tipodeinforme=tipoinf)
        serializer = InformesCajaChicaSerializer(dep, many=True)
        return Response(serializer.data)


class BuscaEstadoInformesCajaChica(APIView):
    def get(self, request, estadoinf, format=None):
        dep = InformesCajaChica.objects.filter(cc_estado=estadoinf)
        serializer = InformesCajaChicaSerializer(dep, many=True)
        return Response(serializer.data)


class BuscaEstadoFondo(APIView):
    def get_object(self, pk):
        try:
            return Fondos.objects.get(pk=pk)
        except Fondos.DoesNotExist:
            raise Http404

    def get(self, request, estadoFon, format=None):
        dep = Fondos.objects.filter(fon_estado=estadoFon)
        serializer = FondosSerializer(dep, many=True)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        fon = self.get_object(pk)
        serializer = FondosSerializer(fon, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BuscaGastoInforme(APIView):
    def get(self, request, gastoInf, format=None):
        dep = Gastos.objects.filter(gas_informe_cc=gastoInf)
        serializer = GastosSerializer(dep, many=True)
        return Response(serializer.data)


class BuscaGastoEstado(APIView):
    def get(self, request, gastoEstado, format=None):
        est = Gastos.objects.filter(gas_estado=gastoEstado)
        serializer = GastosSerializer(est, many=True)
        return Response(serializer.data)
