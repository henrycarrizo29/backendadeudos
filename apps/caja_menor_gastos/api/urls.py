from django.contrib import admin
from django.urls import path, include

app_name = 'caja_menor_gastos'
urlpatterns = [
    path('v1/', include("apps.caja_menor_gastos.api.v1.urls", namespace="v1")),
]
