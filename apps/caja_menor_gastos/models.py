from django.db import models
from ..personal.models import Personal
from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import ArrayField

User = get_user_model()

ESTADO_FONDOS = (
    ('ABIERTO', 'Abierto'),
    ('CERRADO', 'Cerrado'),
)

ESTADO_GASTO = (
    ('ABIERTO', 'Abierto'),
    ('CERRADO', 'Cerrado'),
)

ESTADO_INFOGASTO = (
    ('REVISADO', 'Revisado'),
    ('PENDIENTE', 'Pendiente'),
    ('ALTA', 'Alta'),
)

ESTADO_INFORME = (
    ('ENVIADO', 'Enviado'),
    ('APROBADO', 'Aprobado'),
    ('ASIGNADO', 'Asignado'),
    ('RECHAZADO', 'Rechazado'),
    ('EN_INFORME', 'En_Informe'),
)


class Categoria (models.Model):
    cat_descripcion = models.CharField(max_length=200, verbose_name='Descripcion')

    class Meta:
        ordering = ["cat_descripcion"]

    def __str__(self):
        return str(self.cat_descripcion)


''' class Detalle (models.Model):
    det_descripcion = models.CharField(max_length=200, verbose_name='Descripcion')
    det_categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)

    class Meta:
        ordering = ["det_descripcion"]

    def __str__(self):
        return str(self.det_descripcion) '''


class InformesCajaChica (models.Model):
    cc_tipodeinforme = models.CharField(max_length=100, verbose_name='Tipo de informe')
    cc_receptor = models.CharField(max_length=100, verbose_name='A')
    cc_receptorcargo = models.CharField(max_length=100, blank=True, verbose_name='cargo receptor')
    cc_emisor1 = models.CharField(max_length=100, verbose_name='De')
    cc_emisorcargo1 = models.CharField(max_length=100, blank=True, verbose_name='cargo emisor')
    cc_emisor2 = models.CharField(max_length=100, blank=True, verbose_name='De')
    cc_emisorcargo2 = models.CharField(max_length=100, blank=True, verbose_name='cargo emisor')
    cc_referencia = models.CharField(max_length=240, verbose_name='Referencia')
    cc_fecha = models.DateField(verbose_name="fecha creacion informe")
    cc_contenido = models.TextField(verbose_name='Contenido')
    cc_estado = models.CharField(max_length=10, choices=ESTADO_INFORME, default='EN_INFORME')
    cc_observacion = models.CharField(max_length=240, blank=True, null=True, verbose_name='observacion')
    cc_fecha_aprobación = models.DateField(blank=True, null=True, verbose_name="fecha aprobacion")
    cc_fecha_asignacion = models.DateField(blank=True, null=True, verbose_name="fecha asignacion")

    class Meta:
        ordering = ["-cc_fecha"]

    def __str__(self):
        return '%s' % (self.cc_tipodeinforme)


class InformeGasto (models.Model):
    infogasto_estado = models.CharField(max_length=9, choices=ESTADO_INFOGASTO, default='PENDIENTE')
    infogasto_vec = ArrayField(models.PositiveIntegerField())
    infogasto_informe_cc = models.ForeignKey(InformesCajaChica, blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        ordering = ["id"]

    def __str__(self):
        return str(self.infogasto_estado)


class Fondos (models.Model):
    fon_solicitante = models.ForeignKey(Personal, on_delete=models.CASCADE)
    fon_fecha = models.DateField(verbose_name="fecha asignacion de fondos")
    fon_categoria = models.ForeignKey(Categoria, blank=True, null=True, on_delete=models.SET_NULL)
    fon_detalle = models.CharField(max_length=240, null=True, blank=True, verbose_name='detalle fondos')
    fon_asignado = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='monto asignado')
    fon_gastado = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='monto de descuento para gastos')
    fon_saldo = models.DecimalField(max_digits=12, decimal_places=2, null=True, blank=True, verbose_name='saldo')
    fon_factura = models.CharField(max_length=100, null=True, blank=True)
    fon_recibo = models.CharField(max_length=100, null=True, blank=True)
    fon_archivo = models.FileField(null=True, blank=True)
    fon_nota = models.CharField(max_length=240, null=True, blank=True)
    fon_estado = models.CharField(max_length=7, choices=ESTADO_FONDOS, default='ABIERTO')
    fon_usuario = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Usuario')
    fon_informecajachica = models.ForeignKey(InformesCajaChica, blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        ordering = ["-fon_fecha"]

    def __str__(self):
        return str(self.fon_solicitante)


class Gastos (models.Model):
    gas_solicitante = models.ForeignKey(Personal, on_delete=models.CASCADE)
    gas_fondos = models.ForeignKey(Fondos, on_delete=models.CASCADE)
    gas_fecha = models.DateField(verbose_name="fecha desembolso")
    gas_total = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='monto total')
    gas_categoria = models.ForeignKey(Categoria, blank=True, null=True, on_delete=models.SET_NULL)
    gas_detalle = models.CharField(max_length=240,  blank=True, null=True,verbose_name='detalle gasto')
    gas_factura = models.CharField(max_length=100, null=True, blank=True)
    gas_recibo = models.CharField(max_length=100, null=True, blank=True)
    gas_archivo = models.FileField(null=True, blank=True)
    gas_nota = models.CharField(max_length=240, null=True, blank=True)
    gas_estado = models.CharField(max_length=7, choices=ESTADO_GASTO, default='ABIERTO')
    gas_informe_cc=models.ForeignKey(InformesCajaChica, blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        ordering = ["gas_categoria","-gas_fecha"]

    def __str__(self):
        return str(self.gas_solicitante)


''' class InformeGastos (models.Model):
    inf_cdp = models.ForeignKey(Compromiso_de_pago, on_delete=models.CASCADE)
    inf_receptor = models.CharField(max_length=100, verbose_name='A')
    inf_receptorcargo = models.CharField(max_length=100, blank=True, verbose_name='cargo receptor')
    inf_emisor = models.CharField(max_length=100, verbose_name='De')
    inf_emisorcargo = models.CharField(max_length=100, blank=True, verbose_name='cargo emisor')
    inf_referencia = models.CharField(max_length=240, verbose_name='Referencia')
    inf_fecha = models.DateField()
    inf_contenido = models.TextField(verbose_name='Contenido')

    class Meta:
        ordering = ["-inf_fecha"]

    def __str__(self):
        return '%s' % (self.inf_cdp)
 '''
