from django.contrib import admin
from .models import Categoria, Fondos, Gastos, InformeGasto, InformesCajaChica

# Register your models here.


class FondosAdmin(admin.ModelAdmin):
    # list_display controla que columnas aparecen en la lista de cambios. Siempre y cuando estos nombres de campos, existan en el modelo
    list_display = ('fon_solicitante', 'fon_fecha', 'fon_detalle', 'fon_asignado',
                    'fon_gastado', 'fon_saldo', 'fon_factura', 'fon_recibo', 'fon_archivo', 'fon_nota', 'fon_estado', 'fon_usuario', 'fon_informecajachica')


class GastosAdmin(admin.ModelAdmin):
    # list_display controla que columnas aparecen en la lista de cambios. Siempre y cuando estos nombres de campos, existan en el modelo
    list_display = ('gas_solicitante', 'gas_fondos', 'gas_fecha', 'gas_total', 'gas_categoria',
                    'gas_detalle', 'gas_factura', 'gas_recibo', 'gas_archivo', 'gas_nota', 'gas_estado', 'gas_informe_cc')


class InformeGastoAdmin(admin.ModelAdmin):
    # list_display controla que columnas aparecen en la lista de cambios. Siempre y cuando estos nombres de campos, existan en el modelo
    list_display = ('infogasto_estado', 'infogasto_vec', 'infogasto_informe_cc')


class InformeCajaChicaAdmin(admin.ModelAdmin):
    # list_display controla que columnas aparecen en la lista de cambios. Siempre y cuando estos nombres de campos, existan en el modelo
    list_display = ('cc_tipodeinforme', 'cc_receptor', 'cc_receptorcargo', 'cc_emisor1', 'cc_emisorcargo1',
                    'cc_emisor2', 'cc_emisorcargo2', 'cc_referencia', 'cc_fecha', 'cc_contenido', 'cc_estado', 'cc_observacion', 'cc_fecha_aprobación', 'cc_fecha_asignacion')


# admin.site.register(Compromiso_de_pago)
admin.site.register(Fondos, FondosAdmin)
admin.site.register(Gastos, GastosAdmin)
admin.site.register(Categoria)
admin.site.register(InformeGasto, InformeGastoAdmin)
admin.site.register(InformesCajaChica, InformeCajaChicaAdmin)
