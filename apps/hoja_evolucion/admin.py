from django.contrib import admin
from .models import HojaEvolucion, ServiciosExamenesMedicamentosEmpleados, ListaPrecios, Cie10

# Register your models here.


class HojaEvolucionAdmin(admin.ModelAdmin):
    # list_display controla que columnas aparecen en la lista de cambios. Siempre y cuando estos nombres de campos, existan en el modelo
    list_display = ('hoja_idcompromiso', 'hoja_medico_acargo', 'hoja_fecha', 'hoja_subjetivos',
                    'hoja_objetivos', 'hoja_motivoconsulta', 'hoja_diagnostico')
    search_fields = ('hoja_idcompromiso', 'hoja_fecha')


class ServiciosExamenesMedicamentosEmpleadosAdmin(admin.ModelAdmin):
    # list_display controla que columnas aparecen en la lista de cambios. Siempre y cuando estos nombres de campos, existan en el modelo
    list_display = ('sem_idcompromiso', 'sem_serviciosprestados', 'sem_cantidad')
    search_fields = ('sem_idcompromiso', 'sem_serviciosprestados')


class ListaPreciosAdmin(admin.ModelAdmin):
    # list_display controla que columnas aparecen en la lista de cambios. Siempre y cuando estos nombres de campos, existan en el modelo
    list_display = ('lista_descripcion', 'lista_costo', 'lista_fechareg')


class Cie10Admin(admin.ModelAdmin):
    list_display = ('cie_codigo', 'cie_descripcion')


admin.site.register(ListaPrecios, ListaPreciosAdmin)
admin.site.register(HojaEvolucion, HojaEvolucionAdmin)
admin.site.register(ServiciosExamenesMedicamentosEmpleados, ServiciosExamenesMedicamentosEmpleadosAdmin)
admin.site.register(Cie10, Cie10Admin)
