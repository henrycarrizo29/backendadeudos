from django.apps import AppConfig


class HojaEvolucionConfig(AppConfig):
    name = 'hoja_evolucion'
