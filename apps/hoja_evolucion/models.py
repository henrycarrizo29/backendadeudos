from django.db import models
from ..personal.models import Personal
from ..compromiso_de_pago.models import Compromiso_de_pago


class Cie10(models.Model):
    cie_codigo = models.CharField(primary_key=True, max_length=50)
    cie_descripcion = models.CharField(max_length=200)

    class Meta:
        ordering = ["cie_codigo"]

    def __str__(self):
        return str(self.cie_codigo)


class ListaPrecios (models.Model):
    lista_id = models.CharField(max_length=20, verbose_name='codigo', null=True, blank=True)
    lista_descripcion = models.CharField(max_length=200, verbose_name='descripcion')
    lista_costo = models.DecimalField(max_digits=7, decimal_places=2, verbose_name='costo')
    lista_fechareg = models.DateField()

    class Meta:
        ordering = ["lista_descripcion"]

    def __str__(self):
        return (str(self.lista_descripcion)+"-"+str(self.lista_costo))


class HojaEvolucion(models.Model):
    hoja_idcompromiso = models.ForeignKey(Compromiso_de_pago, on_delete=models.CASCADE)
    hoja_medico_acargo = models.ForeignKey(Personal, on_delete=models.CASCADE)
    hoja_fecha = models.DateField()
    hoja_subjetivos = models.TextField(null=True, blank=True, verbose_name='Subjetivo')
    hoja_objetivos = models.TextField(null=True, blank=True, verbose_name='Objetivo')
    hoja_analisis = models.TextField(null=True, blank=True, verbose_name='Analisis')
    hoja_plan = models.TextField(null=True, blank=True, verbose_name='Plan')
    hoja_motivoconsulta = models.CharField(max_length=250, null=True, blank=True, verbose_name='Motivo de Consulta')
    hoja_enfermedadactual = models.CharField(max_length=250, null=True, blank=True, verbose_name='Enfermedad Actual')
    hoja_diagnostico = models.CharField(max_length=250, null=True, blank=True, verbose_name='Diagnostico')

    class Meta:
        ordering = ["-hoja_fecha"]

    def __str__(self):
        return str(self.hoja_fecha)


class ServiciosExamenesMedicamentosEmpleados(models.Model):
    sem_idcompromiso = models.ForeignKey(Compromiso_de_pago, on_delete=models.CASCADE)
    sem_serviciosprestados = models.ForeignKey(ListaPrecios, on_delete=models.CASCADE)
    sem_cantidad = models.PositiveIntegerField(default=1)

    class Meta:
        ordering = ["sem_idcompromiso"]

    def __str__(self):
        return str(self.sem_idcompromiso)
