from django.contrib import admin
from django.urls import path, include

app_name = 'hojaevolucion'
urlpatterns = [
    path('v1/',include("apps.hoja_evolucion.api.v1.urls", namespace="v1")),
]
