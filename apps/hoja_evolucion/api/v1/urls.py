from django.urls import path
from .views import ListaHojaEvolucion, DetalleHojaEvolucion, ListaServiciosExamenesMedicamentosEmpleados, DetalleServiciosExamenesMedicamentosEmpleados, BuscaHojaEvolucion, BuscaServiciosMedicamentosEmpleados, ListaCie10, DetalleCie10, ListaListaPrecios, DetalleListaPrecios

app_name = 'hojaevolucion'
urlpatterns = [
    path('listacie10/', ListaCie10.as_view(), name="Lista Cie10"),
    path('detallecie10/<str:pk>/', DetalleCie10.as_view(), name="Detalle Cie10"),
    path('listaprecio/', ListaListaPrecios.as_view(), name="Lista precios"),
    path('detallelistaprecio/<int:pk>/', DetalleListaPrecios.as_view(), name="Detalle lista precios"),
    path('listahojaevolucion/', ListaHojaEvolucion.as_view(), name="Lista Hoja Evolucion"),
    path('detallehojaevolucion/<int:pk>/', DetalleHojaEvolucion.as_view(), name="Detalle Hoja Evolucion"),
    path('buscahojaevolucion/<str:busca>/', BuscaHojaEvolucion.as_view(), name="Busca Hoja Evolucion"),
    path('listaserviciosexamenesmedicamentos/', ListaServiciosExamenesMedicamentosEmpleados.as_view(),
         name="Lista Servicios Examenes Medicamentos"),
    path('detalleserviciosexamenesmedicamentos/<int:pk>/',
         DetalleServiciosExamenesMedicamentosEmpleados.as_view(), name="Detalle Servicios Examenes Medicamentos"),
    path('buscaserviciosmedicamentosempleados/<int:busca>/',
         BuscaServiciosMedicamentosEmpleados.as_view(), name="Busca Hoja Evolucion"),
]
