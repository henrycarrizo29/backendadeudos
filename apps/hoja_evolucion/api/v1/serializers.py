from rest_framework import serializers
from ....hoja_evolucion.models import HojaEvolucion, ServiciosExamenesMedicamentosEmpleados, Cie10, ListaPrecios


class HojaEvolucionSerializer(serializers.ModelSerializer):
    class Meta:
        model = HojaEvolucion
        fields = '__all__'


class ServiciosExamenesMedicamentosEmpleadosSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiciosExamenesMedicamentosEmpleados
        fields = '__all__'


class Cie10Serializer(serializers.ModelSerializer):
    class Meta:
        model = Cie10
        fields = '__all__'


class ListaPreciosSerializer(serializers.ModelSerializer):
    class Meta:
        model = ListaPrecios
        fields = '__all__'
