from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from django.db.models import Q

from ....hoja_evolucion.models import HojaEvolucion, ServiciosExamenesMedicamentosEmpleados, Cie10, ListaPrecios
from .serializers import HojaEvolucionSerializer, ServiciosExamenesMedicamentosEmpleadosSerializer, Cie10Serializer, ListaPreciosSerializer


class ListaCie10(APIView):
    # LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        cie = Cie10.objects.all()  # [:20]
        serializer = Cie10Serializer(cie, many=True)
        return Response(serializer.data)

     # CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = Cie10Serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalleCie10(APIView):
    def get_object(self, pk):
        try:
            return Cie10.objects.get(pk=pk)
        except Cie10.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        cie = self.get_object(pk)
        serializer = Cie10Serializer(cie)
        return Response(serializer.data)


class ListaHojaEvolucion(APIView):
    # LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        he = HojaEvolucion.objects.all()  # [:20]
        serializer = HojaEvolucionSerializer(he, many=True)
        return Response(serializer.data)

    # CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = HojaEvolucionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalleHojaEvolucion(APIView):
    def get_object(self, pk):
        try:
            return HojaEvolucion.objects.get(pk=pk)
        except HojaEvolucion.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        he = self.get_object(pk)
        serializer = HojaEvolucionSerializer(he)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        he = self.get_object(pk)
        serializer = HojaEvolucionSerializer(he, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        he = self.get_object(pk)
        he.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ListaServiciosExamenesMedicamentosEmpleados(APIView):
     # LISTA TODOS LOS SERVICIOS PRESTADOS
    def get(self, request, format=None):
        sem = ServiciosExamenesMedicamentosEmpleados.objects.all()  # [:20]
        serializer = ServiciosExamenesMedicamentosEmpleadosSerializer(sem, many=True)
        return Response(serializer.data)

     # CREA UN NUEVO REGISTRO, SERVICIOS PRESTADOS
    def post(self, request, format=None):
        serializer = ServiciosExamenesMedicamentosEmpleadosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalleServiciosExamenesMedicamentosEmpleados(APIView):
    def get_object(self, pk):
        try:
            return ServiciosExamenesMedicamentosEmpleados.objects.get(pk=pk)
        except ServiciosExamenesMedicamentosEmpleados.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        sem = self.get_object(pk)
        serializer = ServiciosExamenesMedicamentosEmpleadosSerializer(sem)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        sem = self.get_object(pk)
        serializer = ServiciosExamenesMedicamentosEmpleadosSerializer(sem, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        sem = self.get_object(pk)
        sem.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ListaListaPrecios(APIView):
    def get(self, request, format=None):
        lp = ListaPrecios.objects.all()  # [:20]
        serializer = ListaPreciosSerializer(lp, many=True)
        return Response(serializer.data)

  # CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = ListaPreciosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalleListaPrecios(APIView):

    def get_object(self, pk):
        try:
            return ListaPrecios.objects.get(pk=pk)
        except ListaPrecios.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        lp = ListaPrecios.objects.filter(id=pk)
        serializer = ListaPreciosSerializer(lp, many=True)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        lis = self.get_object(pk)
        serializer = ListaPreciosSerializer(lis, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        lis = self.get_object(pk)
        lis.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

###############################################################################


class BuscaHojaEvolucion(APIView):
    def get(self, request, busca, format=None):
        # | Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
        bhe = HojaEvolucion.objects.filter(hoja_idcompromiso=busca)
        serializer = HojaEvolucionSerializer(bhe, many=True)
        return Response(serializer.data)


class BuscaServiciosMedicamentosEmpleados(APIView):
    def get(self, request, busca, format=None):
        # | Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
        bsme = ServiciosExamenesMedicamentosEmpleados.objects.filter(sem_idcompromiso=busca)
        serializer = ServiciosExamenesMedicamentosEmpleadosSerializer(bsme, many=True)
        return Response(serializer.data)
