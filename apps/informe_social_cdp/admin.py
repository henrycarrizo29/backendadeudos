from django.contrib import admin
from .models import InformeSocial

# Register your models here.


class InformeSocialAdmin(admin.ModelAdmin):
    # list_display controla que columnas aparecen en la lista de cambios. Siempre y cuando estos nombres de campos, existan en el modelo
    list_display = ('is_cdp', 'is_codigo', 'is_fecha', 'is_contenido','is_descuento', 'is_aceptado','is_observaciones')
    search_fields = ('is_cdp', 'is_fecha')


admin.site.register(InformeSocial, InformeSocialAdmin)
