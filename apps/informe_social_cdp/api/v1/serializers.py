from rest_framework import serializers
from ....informe_social_cdp.models import InformeSocial

class InformeSocialSerializer(serializers.ModelSerializer):
    class Meta:
        model = InformeSocial
        fields = '__all__'
