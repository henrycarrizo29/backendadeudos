from django.urls import path
from .views import ListaInformeSocial, DetalleInformeSocial, InformeSocialBusca

app_name = 'informesocial'
urlpatterns = [
    path('listainformesocial/', ListaInformeSocial.as_view(),name="Lista Informe Social"),
    path('detalleinformesocial/<int:pk>/', DetalleInformeSocial.as_view(), name="Detalle Informe Social"),
    path('informesocialbusca/<int:busca>/', InformeSocialBusca.as_view(), name="Informe Social Busca"),
]