from rest_framework.parsers import FileUploadParser
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from django.db.models import Q
from ....informe_social_cdp.models import InformeSocial
from .serializers import InformeSocialSerializer

class ListaInformeSocial(APIView):
    parser_class = (FileUploadParser,)
    # LISTA TODOS LOS REGISTROS 
    def get(self, request,format=None):
        informe = InformeSocial.objects.all()#[:20]
        serializer = InformeSocialSerializer(informe, many=True)
        return Response(serializer.data)
    
    #CREA UN NUEVO REGISTRO 
    def post(self, request, format=None):
        serializer = InformeSocialSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class DetalleInformeSocial(APIView):
    def get_object(self, pk):
        try:
            return InformeSocial.objects.get(pk=pk)
        except InformeSocial.DoesNotExist:
            raise Http404

    #DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        informe = self.get_object(pk)
        serializer = InformeSocialSerializer(informe)
        return Response(serializer.data)
    
    #ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        informe = self.get_object(pk)
        serializer = InformeSocialSerializer(informe, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        informe = self.get_object(pk)
        informe.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class InformeSocialBusca(APIView):
    def get(self, request, busca, format=None):
        informebusca=InformeSocial.objects.filter(is_cdp=busca) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
        serializer = InformeSocialSerializer(informebusca, many=True)
        return Response(serializer.data)
