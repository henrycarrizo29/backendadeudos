from django.contrib import admin
from django.urls import path, include

app_name = 'informesocial'
urlpatterns = [
    path('v1/',include("apps.informe_social_cdp.api.v1.urls", namespace="v1")),
]
