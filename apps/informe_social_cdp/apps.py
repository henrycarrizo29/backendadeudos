from django.apps import AppConfig


class InformeSocialCdpConfig(AppConfig):
    name = 'informe_social_cdp'
