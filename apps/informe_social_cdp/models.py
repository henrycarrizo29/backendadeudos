from django.db import models
from ..compromiso_de_pago.models import Compromiso_de_pago


class InformeSocial(models.Model):
    is_cdp = models.ForeignKey(Compromiso_de_pago, on_delete=models.CASCADE)
    is_codigo = models.CharField(max_length=30, null=True, blank=True, verbose_name='Codigo de informe')
    is_fecha = models.DateField()
    is_contenido = models.FileField(blank=True, null=True, verbose_name='contenido')
    is_descuento = models.PositiveIntegerField(default=0, verbose_name='descuento')
    is_aceptado = models.BooleanField(default=False, verbose_name="aceptado")
    is_observaciones = models.CharField(max_length=200, null=True, blank=True, verbose_name='Observaciones')

    class Meta:
        ordering = ["is_fecha"]

    def __str__(self):
        return str(self.is_cdp)
