from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status

from ....informe_cdp.models import InformeCDP
from .serializers import InformeSerializer


class ListaInforme(APIView):
    # LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        informe = InformeCDP.objects.all()
        serializer = InformeSerializer(informe, many=True)
        return Response(serializer.data)

    # CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = InformeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalleInforme(APIView):

    def get_object(self, pk):
        try:
            return InformeCDP.objects.get(pk=pk)
        except InformeCDP.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        informe = self.get_object(pk)
        serializer = InformeSerializer(informe)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        informe = self.get_object(pk)
        serializer = InformeSerializer(informe, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        informe = self.get_object(pk)
        informe.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class DetalleInformeBusca(APIView):
    def get(self, request, cdp, format=None):
        dep = InformeCDP.objects.filter(inf_cdp=cdp)
        serializer = InformeSerializer(dep, many=True)
        return Response(serializer.data)
