from django.urls import path
from .views import ListaInforme, DetalleInforme, DetalleInformeBusca

app_name = 'informecdp'
urlpatterns = [
    path('listainforme', ListaInforme.as_view()),
    path('detalleinforme/<int:pk>/', DetalleInforme.as_view(), name="detalle informe"),
    path('detalleinformebusca/<int:cdp>/', DetalleInformeBusca.as_view(), name="detalle informe busca"),
]