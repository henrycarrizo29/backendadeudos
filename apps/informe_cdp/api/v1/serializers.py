from rest_framework import serializers
from ....informe_cdp.models import InformeCDP

class InformeSerializer(serializers.ModelSerializer):

    class Meta:
        model = InformeCDP
        fields = '__all__'
