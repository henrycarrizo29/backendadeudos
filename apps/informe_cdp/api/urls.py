from django.contrib import admin
from django.urls import path, include

app_name = 'informecdp'
urlpatterns = [
    path('v1/',include("apps.informe_cdp.api.v1.urls", namespace="v1")),
]
