from django.db import models
from ..compromiso_de_pago.models import Compromiso_de_pago


class InformeCDP (models.Model):
    inf_cdp = models.ForeignKey(Compromiso_de_pago, on_delete=models.CASCADE)
    inf_receptor = models.CharField(max_length=100, verbose_name='A')
    inf_receptorcargo = models.CharField(max_length=100, blank=True, verbose_name='cargo receptor')
    inf_emisor = models.CharField(max_length=100, verbose_name='De')
    inf_emisorcargo = models.CharField(max_length=100, blank=True, verbose_name='cargo emisor')
    inf_referencia = models.CharField(max_length=240, verbose_name='Referencia')
    inf_fecha = models.DateField()
    inf_contenido = models.TextField(verbose_name='Contenido')

    class Meta:
        ordering = ["-inf_fecha"]

    def __str__(self):
        return '%s' % (self.inf_cdp)
