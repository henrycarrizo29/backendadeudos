from django.contrib import admin
from .models import InformeCDP

# Register your models here.    
class InformeAdmin(admin.ModelAdmin):
    list_display=('inf_cdp','inf_receptor','inf_receptorcargo','inf_emisor','inf_emisorcargo','inf_referencia','inf_fecha','inf_contenido') 
    search_fields = ('inf_cdp','inf_fecha')     

#admin.site.register(InformeCDP)
admin.site.register(InformeCDP, InformeAdmin)
