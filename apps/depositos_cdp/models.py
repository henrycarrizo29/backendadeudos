from django.db import models
from ..compromiso_de_pago.models import Compromiso_de_pago


class Depositos (models.Model):
    dep_cdp = models.ForeignKey(Compromiso_de_pago, on_delete=models.CASCADE)
    dep_fecha_boleta = models.DateField()
    dep_monto_total = models.DecimalField(max_digits=7, decimal_places=2, verbose_name='monto total')
    dep_numero = models.CharField(max_length=30, verbose_name='numero de boleta')
    dep_img_boleta = models.FileField(null=True, blank=True)
    dep_fecha_registro = models.DateField()

    class Meta:
        ordering = ["-dep_fecha_registro"]

    def __str__(self):
        return str(self.dep_fecha_registro)
