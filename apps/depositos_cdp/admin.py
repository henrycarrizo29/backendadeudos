from django.contrib import admin
from .models import Depositos

# Register your models here.    
class DepositosAdmin(admin.ModelAdmin):
    list_display=('dep_cdp','dep_fecha_boleta','dep_monto_total','dep_numero','dep_img_boleta','dep_fecha_registro') 
    search_fields = ('dep_cdp','dep_fecha_registro')     

#admin.site.register(Depositos)
admin.site.register(Depositos, DepositosAdmin)
