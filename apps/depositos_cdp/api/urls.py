from django.contrib import admin
from django.urls import path, include

app_name = 'depositos'
urlpatterns = [
    path('v1/',include("apps.depositos_cdp.api.v1.urls", namespace="v1")),
]
