from rest_framework.parsers import FileUploadParser
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status

from ....depositos_cdp.models import Depositos
from .serializers import DepositosSerializer


class ListaDepositos(APIView):
    parser_class = (FileUploadParser,)

    def post(self, request, *args, **kwargs):
        file_serializer = DepositosSerializer(data=request.data)

        if file_serializer.is_valid():
            file_serializer.save()
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        depositos = Depositos.objects.all()
        serializer = DepositosSerializer(depositos, many=True)
        return Response(serializer.data)


class DetalleDepositos(APIView):

    def get_object(self, pk):
        try:
            return Depositos.objects.get(pk=pk)
        except Depositos.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        depositos = self.get_object(pk)
        serializer = DepositosSerializer(depositos)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        depositos = self.get_object(pk)
        serializer = DepositosSerializer(depositos, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        depositos = self.get_object(pk)
        depositos.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class DetalleDepositoBusca(APIView):
    def get(self, request, cdp, format=None):
        dep = Depositos.objects.filter(dep_cdp=cdp)
        serializer = DepositosSerializer(dep, many=True)
        return Response(serializer.data)
