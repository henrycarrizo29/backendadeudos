from rest_framework import serializers
from ....depositos_cdp.models import Depositos

class DepositosSerializer(serializers.ModelSerializer):

    class Meta:
        model = Depositos
        fields = '__all__'
