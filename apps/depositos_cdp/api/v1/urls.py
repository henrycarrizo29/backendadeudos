from django.urls import path
from .views import ListaDepositos, DetalleDepositos, DetalleDepositoBusca

app_name = 'depositos'
urlpatterns = [
    path('listadepositos', ListaDepositos.as_view()),
    path('detalledepositos/<int:pk>/', DetalleDepositos.as_view(), name="detalle depositos"),
    path('detalledepositobusca/<int:cdp>/', DetalleDepositoBusca.as_view(), name="detalle deposito busca"),
]