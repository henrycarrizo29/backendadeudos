from django.apps import AppConfig


class DepositosCdpConfig(AppConfig):
    name = 'depositos_cdp'
