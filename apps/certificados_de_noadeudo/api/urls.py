from django.contrib import admin
from django.urls import path, include

app_name = 'certificados_de_noadeudo'
urlpatterns = [
    path('v1/', include("apps.certificados_de_noadeudo.api.v1.urls", namespace="v1")),
]
