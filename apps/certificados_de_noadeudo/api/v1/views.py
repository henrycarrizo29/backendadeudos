from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status

from ....certificados_de_noadeudo.models import Certificados, Motivos
from .serializers import CertificadosSerializer, MotivosSerializer


class ListaMotivo(APIView):
    def get(self, request, format=None):
        motivo = Motivos.objects.all()
        serializer = MotivosSerializer(motivo, many=True)
        return Response(serializer.data)

    # CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = MotivosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalleMotivo(APIView):

    def get_object(self, pk):
        try:
            return Motivos.objects.get(pk=pk)
        except Motivos.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO

    def get(self, request, pk, format=None):
        motivo = Motivos.objects.filter(id=pk)
        serializer = MotivosSerializer(motivo, many=True)
        return Response(serializer.data)


class ListaCertificados(APIView):
    # LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        informe = Certificados.objects.all()
        serializer = CertificadosSerializer(informe, many=True)
        return Response(serializer.data)

    # CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = CertificadosSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalleCertificados(APIView):

    def get_object(self, pk):
        try:
            return Certificados.objects.get(pk=pk)
        except Certificados.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        informe = self.get_object(pk)
        serializer = CertificadosSerializer(informe)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        informe = self.get_object(pk)
        serializer = CertificadosSerializer(informe, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        informe = self.get_object(pk)
        informe.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class DetalleCertificadoBusca(APIView):
    def get(self, request, ci, format=None):
        dep = Certificados.objects.filter(cer_ciestudiante=ci)
        serializer = CertificadosSerializer(dep, many=True)
        return Response(serializer.data)
