from django.urls import path
from .views import ListaCertificados, DetalleCertificados, DetalleCertificadoBusca, ListaMotivo, DetalleMotivo

app_name = 'certificados_de_noadeudo'
urlpatterns = [
    path('listacertificados', ListaCertificados.as_view()),
    path('detallecertificados/<int:pk>/', DetalleCertificados.as_view(), name="detalle certificado"),
    path('detallecertificadosbusca/<str:ci>/', DetalleCertificadoBusca.as_view(), name="detalle certificado busca"),
    path('listamotivo', ListaMotivo.as_view(), name="lista motivo"),
    path('detallemotivo/<int:pk>/', DetalleMotivo.as_view(), name="detalle motivo"),
]
