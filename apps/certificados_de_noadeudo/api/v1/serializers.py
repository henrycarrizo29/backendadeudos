from rest_framework import serializers
from ....certificados_de_noadeudo.models import Certificados, Motivos


class CertificadosSerializer(serializers.ModelSerializer):

    class Meta:
        model = Certificados
        fields = '__all__'


class MotivosSerializer(serializers.ModelSerializer):

    class Meta:
        model = Motivos
        fields = '__all__'
