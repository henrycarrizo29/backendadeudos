from django.apps import AppConfig


class CertificadosDeNoadeudoConfig(AppConfig):
    name = 'certificados_de_noadeudo'
