from django.db import models
from ..estudiante.models import Estudiante
from ..personal.models import Personal
from django.contrib.auth import get_user_model

User = get_user_model()


class Motivos(models.Model):
    motivo = models.CharField(max_length=150)

    class Meta:
        ordering = ["motivo"]

    def __str__(self):
        return str(self.motivo)


class Certificados (models.Model):
    cer_ciestudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE,  verbose_name='Estudiante')
    cer_usuario = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Usuario')
    cer_motivo = models.ForeignKey(Motivos, on_delete=models.CASCADE, verbose_name='id_Motivo')   
    cer_fecha = models.DateField(verbose_name='Fecha')
    cer_observacion = models.TextField(blank=True, verbose_name='Observacion')

    class Meta:
        ordering = ["-id"]

    def __str__(self):
        return '%s' % (self.cer_ciestudiante)
