from django.contrib import admin
from .models import Certificados, Motivos

# Register your models here.


class CertificadosAdmin(admin.ModelAdmin):
    list_display = ('cer_ciestudiante', 'cer_usuario', 'cer_motivo', 'cer_fecha', 'cer_observacion')
    search_fields = ('cer_ciestudiante', 'cer_usuario')


# admin.site.register(Certificados)
admin.site.register(Motivos)
admin.site.register(Certificados, CertificadosAdmin)
