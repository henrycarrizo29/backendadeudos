from django.apps import AppConfig


class CompromisoDePagoConfig(AppConfig):
    name = 'compromiso_de_pago'
