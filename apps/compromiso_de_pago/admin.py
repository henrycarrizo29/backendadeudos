from django.contrib import admin
from .models import Compromiso_de_pago, Parentesco

# Register your models here.


class Compromiso_de_pagoAdmin(admin.ModelAdmin):
    # list_display controla que columnas aparecen en la lista de cambios. Siempre y cuando estos nombres de campos, existan en el modelo
    list_display = ('com_estudiante', 'com_garante', 'com_parentesco', 'com_fecha',
                    'com_medico_acargo', 'com_estado', 'com_nroinforme', 'com_observacion', 'com_usuario')
    #search_fields = ('com_estudiante', 'com_fecha')


# admin.site.register(Compromiso_de_pago)
admin.site.register(Compromiso_de_pago, Compromiso_de_pagoAdmin)
admin.site.register(Parentesco)
