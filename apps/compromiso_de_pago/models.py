from django.db import models
from ..estudiante.models import Estudiante
from ..garante.models import Garante
from ..personal.models import Personal
from django.contrib.auth import get_user_model

User = get_user_model()


ESTADO_CHOICES = (
    ('PENDIENTE', 'Pendiente'),
    ('CANCELADO', 'Cancelado'),
)


class Parentesco(models.Model):
    parentesco = models.CharField(max_length=50)

    class Meta:
        ordering = ["parentesco"]

    def __str__(self):
        return str(self.parentesco)


class Compromiso_de_pago(models.Model):
    com_estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)
    com_usuario = models.ForeignKey(User, on_delete=models.CASCADE,verbose_name='Usuario')
    com_garante = models.ForeignKey(Garante, on_delete=models.CASCADE)
    com_parentesco = models.ForeignKey(Parentesco, on_delete=models.CASCADE)
    com_fecha = models.DateField()
    com_medico_acargo = models.ForeignKey(Personal, on_delete=models.SET_NULL, blank=True, null=True)
    com_estado = models.CharField(max_length=9, choices=ESTADO_CHOICES, default='PENDIENTE')
    com_nroinforme = models.CharField(max_length=50, null=True, blank=True)
    com_observacion = models.CharField(max_length=250, null=True, blank=True)

    class Meta:
        ordering = ["-com_fecha"]

    def __str__(self):
        return str(self.com_estudiante)
