from django.urls import path
from .views import ListaCompromiso, DetalleCompromiso, DetalleCompromisoBusca, ListaParentesco, DetalleParentesco

app_name = 'compromiso_de_pago'
urlpatterns = [
    path('listacompromiso', ListaCompromiso.as_view(), name="lista compromiso"),
    path('detallecompromiso/<int:pk>/', DetalleCompromiso.as_view(), name="detalle compromiso"),
    path('detallecompromisobusca/<str:busca>/', DetalleCompromisoBusca.as_view(), name="detalle compromiso busca"),
    path('listaparentesco', ListaParentesco.as_view(), name="lista parentesco"),
    path('detalleparentesco/<int:pk>/', DetalleParentesco.as_view(), name="detalle parentesco"),
]
