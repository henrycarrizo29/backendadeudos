# compromiso_de_pago/serializers.py
from rest_framework import serializers
from ....compromiso_de_pago.models import Compromiso_de_pago, Parentesco


class CompromisoDePagoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Compromiso_de_pago
        fields = '__all__'


class ParentescoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Parentesco
        fields = '__all__'
