from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status

from ....compromiso_de_pago.models import Compromiso_de_pago, Parentesco
from .serializers import CompromisoDePagoSerializer, ParentescoSerializer


class ListaCompromiso(APIView):
    # LISTA TODOS LOS REGISTROS DE LOS ESTUDIANTES
    def get(self, request, format=None):
        cdp = Compromiso_de_pago.objects.all()
        serializer = CompromisoDePagoSerializer(cdp, many=True)
        return Response(serializer.data)

    # CREA UN NUEVO REGISTRO ESTUDIANTE
    def post(self, request, format=None):
        serializer = CompromisoDePagoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalleCompromiso(APIView):

    def get_object(self, pk):
        try:
            return Compromiso_de_pago.objects.get(pk=pk)
        except Compromiso_de_pago.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        cdp = self.get_object(pk)
        serializer = CompromisoDePagoSerializer(cdp)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        cdp = self.get_object(pk)
        serializer = CompromisoDePagoSerializer(cdp, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        cdp = self.get_object(pk)
        cdp.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# copia

class DetalleCompromisoBusca(APIView):
    def get(self, request, busca, format=None):
        # | Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
        comp = Compromiso_de_pago.objects.filter(com_estudiante=busca)
        serializer = CompromisoDePagoSerializer(comp, many=True)
        return Response(serializer.data)


class ListaParentesco(APIView):
    def get(self, request, format=None):
        parentesco = Parentesco.objects.all()
        serializer = ParentescoSerializer(parentesco, many=True)
        return Response(serializer.data)

    # CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = ParentescoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalleParentesco(APIView):

    def get_object(self, pk):
        try:
            return Parentesco.objects.get(pk=pk)
        except Parentesco.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO

    def get(self, request, pk, format=None):
        parentesco = Parentesco.objects.filter(id=pk)
        serializer = ParentescoSerializer(parentesco, many=True)
        return Response(serializer.data)
