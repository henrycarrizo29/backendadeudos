from django.contrib import admin
from django.urls import path, include

app_name = 'compromiso_de_pago'
urlpatterns = [
    path('v1/', include("apps.compromiso_de_pago.api.v1.urls", namespace="v1")),
]
