from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Unidad (models.Model):
    descripcion = models.CharField(max_length=50, verbose_name='nombre_de_unidad')

    def __str__(self):
        return (str(self.descripcion))


class T_Especialidad(models.Model):
    especialidad = models.CharField(max_length=30)

    def __str__(self):
        return (str(self.especialidad))


class Personal(models.Model):
    ci = models.CharField(primary_key=True, max_length=15)
    nombres = models.CharField(max_length=30)
    primer_apellido = models.CharField(max_length=30, blank=True)
    segundo_apellido = models.CharField(max_length=30, blank=True)
    Genero_CHOICES = (
        ('Femenino', 'Femenino'),
        ('Masculino', 'Masculino')
    )
    genero = models.CharField(max_length=20, choices=Genero_CHOICES, default='', blank=True)
    item = models.CharField(max_length=20, blank=True)
    sector = models.CharField(max_length=50)
    responsable = models.BooleanField(default=False)
    cargo = models.CharField(max_length=50)
    carga_horaria = models.CharField(max_length=20)
    Estado_CHOICES = (
        ('Activo', 'Activo'),
        ('Inactivo', 'Inactivo')
    )
    estado = models.CharField(max_length=15, choices=Estado_CHOICES, default='Inactivo', blank=True)
    foto_perfil = models.ImageField(upload_to='perfiles', blank=True, null=True)
    unidad = models.ForeignKey(Unidad, on_delete=models.CASCADE)
    # si se elimina el usuario se eliminara todo lo que se guardo con el
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    especialidad = models.ForeignKey(T_Especialidad, on_delete=models.SET_NULL, blank=True, null=True)

    def _str_(self):
        return '%s %s' % (self.nombres, self.primer_apellido)

    class Meta:
        verbose_name_plural = ("Personal PROMES")
