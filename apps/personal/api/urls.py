from django.contrib import admin
from django.urls import path, include

app_name = 'personal'
urlpatterns = [
    path('v1/',include("apps.personal.api.v1.urls", namespace="v1")),
]