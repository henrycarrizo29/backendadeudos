from django.urls import path
from .views import ListaPersonal, DetallePersonal, ListaUnidad, DetalleUnidad, DetalleUsuario

app_name = 'personal'
urlpatterns = [
    path('listapersonal/', ListaPersonal.as_view(), name="Lista Personal"),
    path('detallepersonal/<str:pk>/', DetallePersonal.as_view(), name="detalle Personal"),
    path('listaunidad/', ListaUnidad.as_view(), name="Lista Unidad"),
    path('detalleunidad/<int:pk>/', DetalleUnidad.as_view(), name="detalle unidad"),
    path('detalleusuario/<int:usr>/', DetalleUsuario.as_view(), name="detalle usuario"),
]
