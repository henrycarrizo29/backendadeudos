from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status

from ....personal.models import Personal, Unidad
from .serializers import PersonalSerializer, UnidadSerializer
# Create your views here.


class ListaPersonal(APIView):
    def get(self, request, format=None):
        personal = Personal.objects.all()
        serializer = PersonalSerializer(personal, many=True)
        return Response(serializer.data)

    # CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = PersonalSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetallePersonal(APIView):

    def get_object(self, pk):
        try:
            return Personal.objects.get(pk=pk)
        except Personal.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO.
    def get(self, request, pk, format=None):
        personal = Personal.objects.filter(ci=pk)
        serializer = PersonalSerializer(personal, many=True)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        pe = self.get_object(pk)
        serializer = PersonalSerializer(pe, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ListaUnidad(APIView):
    def get(self, request, format=None):
        unidad = Unidad.objects.all()
        serializer = UnidadSerializer(unidad, many=True)
        return Response(serializer.data)

    # CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = UnidadSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalleUnidad(APIView):

    def get_object(self, pk):
        try:
            return Unidad.objects.get(pk=pk)
        except Unidad.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO.
    def get(self, request, pk, format=None):
        unidad = Unidad.objects.filter(id=pk)
        serializer = UnidadSerializer(unidad, many=True)
        return Response(serializer.data)


class DetalleUsuario(APIView):
    def get(self, request, usr, format=None):
        Formulario = Personal.objects.all().filter(usuario=usr)
        serializer = PersonalSerializer(Formulario, many=True)
        return Response(serializer.data)
