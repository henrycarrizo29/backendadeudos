from django.contrib import admin
from .models import Personal, Unidad, T_Especialidad

# Register your models here.


class PersonalAdmin(admin.ModelAdmin):
    # list_display controla que columnas aparecen en la lista de cambios. Siempre y cuando estos nombres de campos, existan en el modelo
    list_display = ('ci', 'nombres', 'primer_apellido', 'segundo_apellido', 'genero', 'item',
                    'sector', 'responsable', 'cargo', 'carga_horaria', 'estado', 'foto_perfil', 'unidad', 'usuario', 'especialidad')
    search_fields = ('ci', 'nombres')     # agregando una barra de busqueda esta es por nombre y apellidos


# admin.site.register(Medico)
admin.site.register(Personal, PersonalAdmin)
admin.site.register(Unidad)
admin.site.register(T_Especialidad)
