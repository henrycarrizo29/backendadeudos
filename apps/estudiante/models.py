from django.utils import timezone
from django.contrib.postgres.fields import ArrayField
from django.db import models


class Facultad(models.Model):
    facultad = models.CharField(max_length=100, verbose_name='Facultad')

    def __str__(self):
        return '%s' % (self.facultad)

    class Meta:
        verbose_name_plural = ("Facultades")


class Carrera(models.Model):
    carrera = models.CharField(max_length=50, verbose_name='Carrera')
    programa = models.CharField(max_length=15, null=True, blank=True)
    facultad = models.ForeignKey(Facultad, on_delete=models.CASCADE)

    def __str__(self):
        return '%s' % (self.carrera)

    class Meta:
        verbose_name_plural = ("Carreras")


class T_Estado_Civil(models.Model):
    estado = models.CharField(max_length=15, verbose_name='T_Estado_Civil')

    def __str__(self):
        return '%s' % (self.estado)

    class Meta:
        verbose_name_plural = ("T_Estado_Civil")


class Estudiante (models.Model):
    Sexo = (('F', 'Femenino'), ('M', 'Masculino'))
    ci = models.CharField(primary_key=True, max_length=15, )
    nombres = models.CharField(max_length=30)
    primer_apellido = models.CharField(max_length=30)
    segundo_apellido = models.CharField(max_length=30)
    sexo = models.CharField(max_length=1, choices=Sexo)
    fecha_nac = models.DateField()
    telefono = models.IntegerField(null=True, blank=True)
    celular = models.IntegerField(null=True, blank=True)
    direccion = models.CharField(max_length=100)
    email = models.CharField(max_length=25, default='')
    est_civil = models.ForeignKey(T_Estado_Civil, on_delete=models.CASCADE)
    Carrera = models.ForeignKey(Carrera, on_delete=models.CASCADE)
    matricula = models.PositiveIntegerField()

    def __str__(self):
        return '%s' % (self.ci)

    def __unicode__(self):
        return self.ci

    class Meta:
        verbose_name_plural = ("Estudiantes")

################################################


class Estudianteumsa(models.Model):
    dip = models.CharField(max_length=15)
    id_programa = models.CharField(max_length=7)
    id_estudiante = models.IntegerField(primary_key=True)
    primer_apellido = models.CharField(max_length=20, blank=True, null=True)
    segundo_apellido = models.CharField(max_length=20, blank=True, null=True)
    nombres = models.CharField(max_length=30)
    telefono = ArrayField(models.IntegerField(), blank=True)
    correo = models.CharField(max_length=30, blank=True, null=True)
    direccion = models.CharField(max_length=50, blank=True, null=True)
    fec_nacimiento = models.DateField()
    estado_estudiante = models.CharField(max_length=40)
    sede = models.CharField(max_length=20)
    id_sede = models.IntegerField()
    matricula = models.IntegerField()
    id_sexo = models.CharField(max_length=1)
    estado_civil = models.CharField(max_length=10)
    provincia = models.CharField(max_length=30)
    tieneSeguroActual = models.BooleanField()
    gestionSeguro = models.IntegerField()
    tieneCertificadoNoDeudas = models.BooleanField()
    gestionCertificadoNoDeudas = models.IntegerField()

    def __str__(self):
        return self.dip

    class Meta:
        verbose_name_plural = ("EstUMSA")
