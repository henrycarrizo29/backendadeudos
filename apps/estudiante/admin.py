from django.contrib import admin
from .models import Estudiante, Facultad, Carrera, T_Estado_Civil, Estudianteumsa


class EstudianteAdmin(admin.ModelAdmin):
    list_display = ('ci', 'nombres', 'primer_apellido', 'segundo_apellido', 'sexo', 'fecha_nac', 'telefono', 'celular', 'direccion', 'email',
                    'est_civil', 'Carrera', 'matricula')  # list_display controla que columnas aparecen en la lista de cambios. Siempre y cuando estos nombres de campos, existan en el modelo
    search_fields = ('ci', 'nombres')     # agregando una barra de busqueda esta es por nombre y apellidos


class CarreraAdmin(admin.ModelAdmin):
    list_display = ('carrera', 'programa', 'facultad')


admin.site.register(Facultad)
# admin.site.register(Carrera)
admin.site.register(Carrera, CarreraAdmin)
admin.site.register(T_Estado_Civil)
# admin.site.register(Estudiante)
admin.site.register(Estudiante, EstudianteAdmin)
admin.site.register(Estudianteumsa)
