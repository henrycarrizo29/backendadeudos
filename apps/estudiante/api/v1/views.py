from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from ....estudiante.models import Estudiante, Facultad, Carrera, T_Estado_Civil, Estudianteumsa
from .serializers import EstudianteSerializer, FacultadSerializers, CarreraSerializers, T_Estado_CivilSerializers, EstudianteumsaSerializers
from django.db.models import Q


class ListaEstudiante(APIView):
    # LISTA TODOS LOS REGISTROS DE LOS ESTUDIANTES
    def get(self, request, format=None):
        estudiante = Estudiante.objects.all()
        serializer = EstudianteSerializer(estudiante, many=True)
        return Response(serializer.data)

    # CREA UN NUEVO REGISTRO ESTUDIANTE
    def post(self, request, format=None):
        serializer = EstudianteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalleEstudiante(APIView):

    def get_object(self, pk):
        try:
            return Estudiante.objects.get(pk=pk)
        except Estudiante.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        estudiante = self.get_object(pk)
        serializer = EstudianteSerializer(estudiante)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        estudiante = self.get_object(pk)
        serializer = EstudianteSerializer(estudiante, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        estudiante = self.get_object(pk)
        estudiante.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ListaCarrera(APIView):
    def get(self, request, format=None):
        carrera = Carrera.objects.all()
        serializer = CarreraSerializers(carrera, many=True)
        return Response(serializer.data)


class DetalleCarrera(APIView):

    def get_object(self, pk):
        try:
            return Carrera.objects.get(pk=pk)
        except Carrera.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        carrera = Carrera.objects.filter(id=pk)
        serializer = CarreraSerializers(carrera, many=True)
        return Response(serializer.data)


class ListaFacultad(APIView):
    def get(self, request, format=None):
        facultad = Facultad.objects.all()
        serializer = FacultadSerializers(facultad, many=True)
        return Response(serializer.data)


class DetalleFacultad(APIView):

    def get_object(self, pk):
        try:
            return Facultad.objects.get(pk=pk)
        except Facultad.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        facultad = Facultad.objects.filter(id=pk)
        serializer = FacultadSerializers(facultad, many=True)
        return Response(serializer.data)


class ListaEstudianteumsa(APIView):
    # LISTA TODOS LOS REGISTROS DE LOS ESTUDIANTES
    def get(self, request, format=None):
        estudianteu = Estudianteumsa.objects.all()
        serializer = EstudianteumsaSerializers(estudianteu, many=True)
        return Response(serializer.data)

    # CREA UN NUEVO REGISTRO ESTUDIANTE
    def post(self, request, format=None):
        serializer = EstudianteumsaSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalleEstudianteumsa(APIView):

    def get_object(self, pk):
        try:
            return Estudianteumsa.objects.get(pk=pk)
        except Estudianteumsa.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        estudianteu = self.get_object(pk)
        serializer = EstudianteumsaSerializers(estudianteu)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        estudianteu = self.get_object(pk)
        serializer = EstudianteumsaSerializers(estudianteu, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        estudianteu = self.get_object(pk)
        estudianteu.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
