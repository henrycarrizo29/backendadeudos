from rest_framework import serializers
from ....estudiante.models import Estudiante, Facultad, Carrera, T_Estado_Civil, Estudianteumsa


class EstudianteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Estudiante
        fields = '__all__'


class FacultadSerializers(serializers.ModelSerializer):

    class Meta:
        model = Facultad
        fields = '__all__'


class CarreraSerializers(serializers.ModelSerializer):

    class Meta:
        model = Carrera
        fields = '__all__'


class T_Estado_CivilSerializers(serializers.ModelSerializer):

    class Meta:
        model = T_Estado_Civil
        fields = '__all__'


class EstudianteumsaSerializers(serializers.ModelSerializer):

    class Meta:
        model = Estudianteumsa
        fields = '__all__'
