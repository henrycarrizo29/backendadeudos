from django.urls import path
from .views import ListaEstudiante, DetalleEstudiante, ListaCarrera, DetalleCarrera, ListaFacultad, DetalleFacultad, ListaEstudianteumsa, DetalleEstudianteumsa

app_name = 'estudiante'
urlpatterns = [
    path('listaestudiante', ListaEstudiante.as_view()),
    path('detalleestudiante/<str:pk>/', DetalleEstudiante.as_view(), name="detalle estudiante"),
    path('listacarrera/', ListaCarrera.as_view(), name="Lista Carrera"),
    path('detallecarrera/<int:pk>/', DetalleCarrera.as_view(), name="Detalle Carrera"),
    path('listafacultad/', ListaFacultad.as_view(), name="Lista Facultad"),
    path('detallefacultad/<int:pk>/', DetalleFacultad.as_view(), name="Detalle Facultad"),
    path('listaestudianteumsa', ListaEstudianteumsa.as_view()),
    path('detalleestudianteumsa/<int:pk>/', DetalleEstudianteumsa.as_view(), name="detalle estudianteumsa"),
]
