from django.contrib import admin
from django.urls import path, include

app_name = 'estudiante'
urlpatterns = [
    path('v1/', include("apps.estudiante.api.v1.urls", namespace="v1")),
]
