from django.contrib import admin
from .models import Garante


class GaranteAdmin(admin.ModelAdmin):
    # list_display controla que columnas aparecen en la lista de cambios. Siempre y cuando estos nombres de campos, existan en el modelo
    list_display = ('gar_ci', 'gar_ciorigen', 'gar_nombre', 'gar_apellido1', 'gar_apellido2',
                    'gar_genero', 'gar_telefono', 'gar_celular', 'gar_direccion', 'gar_correo')
    search_fields = ('gar_ci', 'gar_nombre')


# admin.site.register(Garante)
admin.site.register(Garante, GaranteAdmin)
