from django.db import models

GENERO_CHOICES = (
    ('M', 'Masculino'),
    ('F', 'Femenino'),
)

CI_CHOICES = (
    ('LP', 'La Paz'),
    ('OR', 'Oruro'),
    ('PT', 'Potosi'),
    ('CB', 'Cochabamba'),
    ('CH', 'Chuquisaca'),
    ('TJ', 'Tarija'),
    ('SC', 'Santa Cruz'),
    ('BN', 'Beni'),
    ('PA', 'Pando'),
)


class Garante (models.Model):
    gar_ci = models.CharField(max_length=15, verbose_name='ci', primary_key=True)
    gar_ciorigen = models.CharField(max_length=2, choices=CI_CHOICES, verbose_name='ci_origen')
    gar_nombre = models.CharField(max_length=30, verbose_name='nombre')
    gar_apellido1 = models.CharField(max_length=30, null=True, blank=True, verbose_name='apellido_1')
    gar_apellido2 = models.CharField(max_length=30, null=True, blank=True, verbose_name='apellido_2')
    gar_genero = models.CharField(max_length=1, choices=GENERO_CHOICES, verbose_name='genero')
    gar_direccion = models.CharField(max_length=250, null=True, blank=True, verbose_name='direccion')
    gar_telefono = models.CharField(max_length=50, null=True, blank=True, verbose_name='telefono')
    gar_celular = models.CharField(max_length=50, null=True, blank=True, verbose_name='celular')
    gar_correo = models.EmailField(null=True, blank=True, verbose_name='e­-mail')

    class Meta:
        ordering = ["gar_ci"]

    def __str__(self):
        return '%s %s %s %s' % (self.gar_ci, self.gar_nombre, self.gar_apellido1, self.gar_apellido2)
