from django.contrib import admin
from django.urls import path, include

app_name = 'garante'
urlpatterns = [
    path('v1/', include("apps.garante.api.v1.urls", namespace="v1")),
]
