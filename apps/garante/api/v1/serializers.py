from rest_framework import serializers
from ....garante.models import Garante


class GaranteSerializers(serializers.ModelSerializer):

    class Meta:
        model = Garante
        fields = '__all__'
