from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from ....garante.models import Garante
from .serializers import GaranteSerializers


class ListaGarante(APIView):
    # LISTA TODOS LOS REGISTROS DE LOS GARANTES
    def get(self, request, format=None):
        garante = Garante.objects.all()
        serializer = GaranteSerializers(garante, many=True)
        return Response(serializer.data)

    # CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = GaranteSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DetalleGarante(APIView):

    def get_object(self, pk):
        try:
            return Garante.objects.get(pk=pk)
        except Garante.DoesNotExist:
            raise Http404

    # DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        garante = self.get_object(pk)
        serializer = GaranteSerializers(garante)
        return Response(serializer.data)

    # ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        garante = self.get_object(pk)
        serializer = GaranteSerializers(garante, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        garante = self.get_object(pk)
        garante.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
