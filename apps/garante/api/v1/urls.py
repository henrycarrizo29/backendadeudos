from django.urls import path
from .views import ListaGarante, DetalleGarante

app_name = 'garante'
urlpatterns = [
    path('listagarante', ListaGarante.as_view()),
    path('detallegarante/<int:pk>/', DetalleGarante.as_view(), name="detalle garante"),
]
