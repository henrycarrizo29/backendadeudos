from django.conf import settings
from django.urls import include, path
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views

from rest_framework_jwt.views import obtain_jwt_token

urlpatterns = [
    path("", TemplateView.as_view(template_name="pages/home.html"), name="home"),
    path(
        "about/", TemplateView.as_view(template_name="pages/about.html"), name="about"
    ),
    # Django Admin, use {% url 'admin:index' %}
    path(settings.ADMIN_URL, admin.site.urls),
    # User management
    path("users/", include("backendadeudospromes.users.urls", namespace="users")),
    path("accounts/", include("allauth.urls")),

    # Your stuff: custom urls includes go here
    path("token-auth/", obtain_jwt_token),
    path("core/", include("apps.usuarios_login.urls")),

    path('adeudos/personal/', include("apps.personal.api.urls", namespace="api-personal")),
    path('adeudos/garante/', include("apps.garante.api.urls", namespace="api-garante")),
    path('adeudos/estudiante/', include("apps.estudiante.api.urls", namespace="api-estudiante")),
    path('adeudos/compromiso_de_pago/', include("apps.compromiso_de_pago.api.urls", namespace="api-compromisodepago")),
    path('adeudos/hoja_evolucion/', include("apps.hoja_evolucion.api.urls", namespace="api-hoja_evolucion")),
    path('adeudos/depositos_cdp/', include("apps.depositos_cdp.api.urls", namespace="api-depositos_cdp")),
    path('adeudos/informe_cdp/', include("apps.informe_cdp.api.urls", namespace="api-informe_cdp")),
    path('adeudos/liquidacion_cdp/', include("apps.liquidacion_cdp.api.urls", namespace="api-liquidacion_cdp")),
    path('adeudos/informe_social_cdp/', include("apps.informe_social_cdp.api.urls", namespace="api-informe_social_cdp")),
    path('adeudos/uploadapp/', include("apps.uploadapp.api.urls", namespace="api-uploadapp")),
    path('adeudos/certificados_de_noadeudo/', include("apps.certificados_de_noadeudo.api.urls", namespace="api-certificados")),
    path('adeudos/caja_menor_gastos/', include("apps.caja_menor_gastos.api.urls", namespace="api-gastos")),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
